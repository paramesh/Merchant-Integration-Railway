import urllib2
import requests
from flask import Flask, jsonify
from flask import abort
from flask import make_response
from flask import request
from flask.json import loads, dumps
from random import randint

app=Flask(__name__)

response_pnr={
    "response_code": 200,
    "error": False,
    "train_name": "NDLS MGS EXP",
    "train_num": "12402",
    "pnr": "6252488859",
    "failure_rate": 19.346153846153847,
    "doj": "27-5-2016",
    "chart_prepared": "N",
    "class": "3A",
    "total_passengers": 1,
    "train_start_date": {
        "month": 5,
        "year": 2016,
        "day": 28
    },
    "from_station": {
        "code": "NDLS",
        "name": "NEW DELHI"
    },
    "boarding_point": {
        "code": "NDLS",
        "name": "NEW DELHI"
    },
    "to_station": {
        "code": "MGS",
        "name": "MUGHALSARAI"
    },
    "reservation_upto": {
        "code": "MGS",
        "name": "MUGHALSARAI"
    },
    "passengers": [
        {
            "no": 1,
            "booking_status": "W/L 4",
            "current_status": "W/L 2",
            
      }
    ]
}

key=['cbwox6746','azjhd3084','wxzls2132','ajase8000']

#function to return PNR status
@app.route('/train/getPnr/') 
def get_pnr_status():
    pnr=request.args.get('pnr')
    if pnr=="6252488859":
        response=response_pnr
    else:
        response=urllib2.urlopen("http://api.railwayapi.com/pnr_status/pnr/"+str(pnr)+"/apikey/"+key[randint(0,3)]+"/")
        response=response.read()

    return jsonify({'result':response})



#function to return Train Route
@app.route('/train/getRoute/')
def train_route():
    boardStn=request.args.get('boardingStn')
    destStn=request.args.get('destinationStn')
    train_no=request.args.get('train_no')
    response=urllib2.urlopen("http://api.railwayapi.com/route/train/"+str(train_no)+"/apikey/"+key[randint(0,3)]+"/")
    #response_route={}
    #with open('response.json','r') as outfile:
     #   response_route=json.load(outfile)
    data=response.read()
    #print type(data)
    res=dict()
    dict_response=loads(data)
    print type(dict_response)
    for stn in dict_response['route']:
        if stn['code']==str(boardStn):
            res['boarding']=dict({'hours':stn['schdep'].split(':')[0].strip('"'),'mins':stn['schdep'].split(':')[1].strip('"'),'day':stn['day']})
        if stn['code']==str(destStn):
            res['destination']=dict({'hours':stn['scharr'].split(':')[0].strip('"'),'mins':stn['scharr'].split(':')[1].strip('"'),'day':stn['day']})
        if stn['scharr']=="Source":
            res['origin']=dict({'hours':stn['schdep'].split(':')[0].strip('"'),'mins':stn['schdep'].split(':')[1].strip('"'),'day':stn['day']})
    print
    print res
    return jsonify({'result':res})


@app.route('/train/stnName/')
def get_stnName():
    stn_code=request.args.get('stn_code')
    response=urllib2.urlopen("http://api.railwayapi.com/code_to_name/code/"+str(stn_code)+"/apikey/"+key[randint(0,3)]+"/")
    dict_response=loads(response.read())
    print dict_response
    return jsonify({'result':dict_response})

def insertChar(mystring, position, chartoinsert ):
    longi = len(mystring)
    mystring   =  mystring[:position] + chartoinsert + mystring[position:] 
    return mystring

@app.route('/train/getStatus/')
def train_status():
    train_no=request.args.get('train_no')
    doj=request.args.get('doj')
    boardStn=request.args.get('boardStn')

    res=dict()

    date=doj
    x=doj[0:4]
    y=doj[4:8]
    y1=y[0:2]
    y2=y[2:4]
    y=y2+y1
    doj=y+x
    doj=insertChar(doj,2,'-')
    doj=insertChar(doj,5,'-')



    response1=urllib2.urlopen("http://api.railwayapi.com/cancelled/date/+"+str(doj)+"/apikey/"+key[randint(0,3)]+"/")
    response1=loads(response1.read())
    #response1={}
    #with open('cancelled.json','r') as outfile1:
    #   response1=json.load(outfile1)

    for train in response1['trains'] :
        train=train['train']
        if train['number'] == str(train_no) :
            res['status']='cancelled'
            return jsonify({'result':res})

    rescheduled=False
    response2=urllib2.urlopen("http://api.railwayapi.com/rescheduled/date/"+str(doj)+"/apikey/"+key+"/")
    response2=loads(response2.read())
    for train in response2['trains'] :
        if train['number'] == str(train_no) :
            res['status']='delayed'
            res['reschedule_date']=train['reschedule_date']
            res['delay_hrs']=train['time_diff'][0:2]
            res['delay_mins']=train['time_diff'][2:4]
            rescheduled=True

    doj=date
    response=urllib2.urlopen("http://api.railwayapi.com/live/train/"+str(train_no)+"/doj/"+str(doj)+"/apikey/"+key[randint(0,3)]+"/")
    response=loads(response.read())

    #response={}
    #with open('live.json','r') as outfile:
    #    response=json.load(outfile)


    #print response
    data=response['position']
    parts=data.split(' ')
    mins=''
    for part in parts :
        if(part.isdigit()):
            mins=part
    #print mins
    if mins=='':
        minutes=0
    else :
        minutes=int(mins)
    hours=minutes/60;
    minutes=minutes%60;

    for stn in response['route']:
        stn1=stn
        stn=stn['station_']
        if stn['code']==str(boardStn):
            res['schdep']=stn1['schdep']

    if rescheduled==True : 
        return jsonify({'result':res})

    if minutes==0 & hours==0 :
        res['status']='ontime' 
    else :
        res['status']='delayed'
        res['delay_hrs']=str(hours)
        res['delay_minutes']=str(minutes)
    print res
    return jsonify({'result':res})

if __name__=='__main__':
    app.run(debug=True,threaded=True,host="0.0.0.0")





